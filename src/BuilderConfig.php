<?php

namespace DeviceDetectVCL;


class BuilderConfig
{
    /**
     * name of generated device detect sub
     *
     * @var string
     */
    protected $deviceDetectSubName          = 'devicedetect';

    /**
     * variable to get User Agent from in built VCL
     *
     * @var string
     */
    protected $userAgentVCLVar              = 'req.http.User-Agent';

    /**
     * records match detected, prevent evaluation of phone/tablet regex after match is found
     *
     * @var string
     */
    protected $detectedVCLVar               = 'req.http.X-UA-Detected';

    /**
     * Browser has been detected
     *
     * @var string
     */
    protected $detectedBrowserVCLVar        = 'req.http.X-UA-Detected-Browser';

    /**
     * OS Detected
     *
     * @var string
     */
    protected $detectedOSVCLVar             = 'req.http.X-UA-Detected-OS';

    /**
     * Utility Detected
     *
     * @var string
     */
    protected $detectedUtilityVCLVar        = 'req.http.X-UA-Detected-Utility';

    /**
     * Phone match
     *
     * @var string
     */
    protected $phoneVCLVar                  = 'req.http.X-UA-Phone';

    /**
     * Phone match version
     *
     * @var string
     */
    protected $phoneVersionVCLVar           = 'req.http.X-UA-Phone-Vs';

    /**
     * Tablet
     *
     * @var string
     */
    protected $tabletVCLVar                 = 'req.http.X-UA-Tablet';

    /**
     * Tablet Version
     *
     * @var string
     */
    protected $tabletVersionVCLVar          = 'req.http.X-UA-Tablet-Vs';

    /**
     * Browser
     *
     * @var string
     */
    protected $browserVCLVar                = 'req.http.X-UA-Browser';

    /**
     * Browser Version
     *
     * @var string
     */
    protected $browserVersionVCLVar         = 'req.http.X-UA-Browser-Vs';

    /**
     * Operating System
     *
     * @var string
     */
    protected $osVCLVar                     = 'req.http.X-UA-Os';

    /**
     * Operating System Version
     *
     * @var string
     */
    protected $osVersionVCLVar              = 'req.http.X-UA-Os-Vs';

    /**
     * Utility
     *
     * @var string
     */
    protected $utilityVCLVar                = 'req.http.X-UA-Utility';

    /**
     * Utility Version
     *
     * @var string
     */
    protected $utilityVersionVCLVar         = 'req.http.X-UA-Utility-Vs';

    /**
     * @return string
     */
    public function getDeviceDetectSubName()
    {
        return $this->deviceDetectSubName;
    }

    /**
     * @param string $deviceDetectSubName
     * @return BuilderConfig
     */
    public function setDeviceDetectSubName($deviceDetectSubName)
    {
        $this->deviceDetectSubName = $deviceDetectSubName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgentVCLVar()
    {
        return $this->userAgentVCLVar;
    }

    /**
     * @param string $userAgentVCLVar
     * @return BuilderConfig
     */
    public function setUserAgentVCLVar($userAgentVCLVar)
    {
        $this->userAgentVCLVar = $userAgentVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetectedVCLVar()
    {
        return $this->detectedVCLVar;
    }

    /**
     * @param string $detectedVCLVar
     * @return BuilderConfig
     */
    public function setDetectedVCLVar($detectedVCLVar)
    {
        $this->detectedVCLVar = $detectedVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetectedBrowserVCLVar()
    {
        return $this->detectedBrowserVCLVar;
    }

    /**
     * @param string $detectedBrowserVCLVar
     * @return BuilderConfig
     */
    public function setDetectedBrowserVCLVar($detectedBrowserVCLVar)
    {
        $this->detectedBrowserVCLVar = $detectedBrowserVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetectedOSVCLVar()
    {
        return $this->detectedOSVCLVar;
    }

    /**
     * @param string $detectedOSVCLVar
     * @return BuilderConfig
     */
    public function setDetectedOSVCLVar($detectedOSVCLVar)
    {
        $this->detectedOSVCLVar = $detectedOSVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetectedUtilityVCLVar()
    {
        return $this->detectedUtilityVCLVar;
    }

    /**
     * @param string $detectedUtilityVCLVar
     * @return BuilderConfig
     */
    public function setDetectedUtilityVCLVar($detectedUtilityVCLVar)
    {
        $this->detectedUtilityVCLVar = $detectedUtilityVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneVCLVar()
    {
        return $this->phoneVCLVar;
    }

    /**
     * @param string $phoneVCLVar
     * @return BuilderConfig
     */
    public function setPhoneVCLVar($phoneVCLVar)
    {
        $this->phoneVCLVar = $phoneVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneVersionVCLVar()
    {
        return $this->phoneVersionVCLVar;
    }

    /**
     * @param string $phoneVersionVCLVar
     * @return BuilderConfig
     */
    public function setPhoneVersionVCLVar($phoneVersionVCLVar)
    {
        $this->phoneVersionVCLVar = $phoneVersionVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getTabletVCLVar()
    {
        return $this->tabletVCLVar;
    }

    /**
     * @param string $tabletVCLVar
     * @return BuilderConfig
     */
    public function setTabletVCLVar($tabletVCLVar)
    {
        $this->tabletVCLVar = $tabletVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getTabletVersionVCLVar()
    {
        return $this->tabletVersionVCLVar;
    }

    /**
     * @param string $tabletVersionVCLVar
     * @return BuilderConfig
     */
    public function setTabletVersionVCLVar($tabletVersionVCLVar)
    {
        $this->tabletVersionVCLVar = $tabletVersionVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrowserVCLVar()
    {
        return $this->browserVCLVar;
    }

    /**
     * @param string $browserVCLVar
     * @return BuilderConfig
     */
    public function setBrowserVCLVar($browserVCLVar)
    {
        $this->browserVCLVar = $browserVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrowserVersionVCLVar()
    {
        return $this->browserVersionVCLVar;
    }

    /**
     * @param string $browserVersionVCLVar
     * @return BuilderConfig
     */
    public function setBrowserVersionVCLVar($browserVersionVCLVar)
    {
        $this->browserVersionVCLVar = $browserVersionVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getOsVCLVar()
    {
        return $this->osVCLVar;
    }

    /**
     * @param string $osVCLVar
     * @return BuilderConfig
     */
    public function setOsVCLVar($osVCLVar)
    {
        $this->osVCLVar = $osVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getOsVersionVCLVar()
    {
        return $this->osVersionVCLVar;
    }

    /**
     * @param string $osVersionVCLVar
     * @return BuilderConfig
     */
    public function setOsVersionVCLVar($osVersionVCLVar)
    {
        $this->osVersionVCLVar = $osVersionVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtilityVCLVar()
    {
        return $this->utilityVCLVar;
    }

    /**
     * @param string $utilityVCLVar
     * @return BuilderConfig
     */
    public function setUtilityVCLVar($utilityVCLVar)
    {
        $this->utilityVCLVar = $utilityVCLVar;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtilityVersionVCLVar()
    {
        return $this->utilityVersionVCLVar;
    }

    /**
     * @param string $utilityVersionVCLVar
     * @return BuilderConfig
     */
    public function setUtilityVersionVCLVar($utilityVersionVCLVar)
    {
        $this->utilityVersionVCLVar = $utilityVersionVCLVar;
        return $this;
    }
}

