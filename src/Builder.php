<?php

namespace DeviceDetectVCL;

class Builder
{
    /**
     * @var BuilderConfig
     */
    protected $config;

    /**
     * @var \DeviceDetectVCL\DetectionProviderInterface
     */
    protected $detectionProvider;

    /**
     * Builder constructor.
     *
     * @param \DeviceDetectVCL\RulesInterface $rules
     */
    public function __construct(BuilderConfig $config, DetectionProviderInterface $detectionProvider)
    {
        $this->config               = $config;
        $this->detectionProvider    = $detectionProvider;
    }

    /**
     * @return BuilderConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return DetectionProviderInterface
     */
    public function getDetectionProvider()
    {
        return $this->detectionProvider;
    }

    /**
     * Generate Sub function
     */
    public function generateSub()
    {
        $config = $this->getConfig();

        //create sub start with default assignments
        $this->generateSubStart($config->getDeviceDetectSubName());

        //generateUsingAgentLibrary vcl to match/is phones
        $this->generateVCL(
            $this->getDetectionProvider()->getAllPhones(),
            $config->getPhoneVCLVar(),
            $config->getPhoneVersionVCLVar(),
            $config->getUserAgentVCLVar(),
            $config->getDetectedVCLVar()
        );

        //generateUsingAgentLibrary vcl to match/is tablet
        $this->generateVCL(
            $this->getDetectionProvider()->getAllTablets(),
            $config->getTabletVCLVar(),
            $config->getTabletVersionVCLVar(),
            $config->getUserAgentVCLVar(),
            $config->getDetectedVCLVar()
        );

        //generateUsingAgentLibrary vcl to match browser
        $this->generateVCL(
            $this->getDetectionProvider()->getAllBrowsers(),
            $config->getBrowserVCLVar(),
            $config->getBrowserVersionVCLVar(),
            $config->getDetectedVCLVar(),
            $config->getDetectedBrowserVCLVar()
        );

        //generateUsingAgentLibrary vcl to match os
        $this->generateVCL(
            $this->getDetectionProvider()->getAllOs(),
            $config->getOsVCLVar(),
            $config->getOsVersionVCLVar(),
            $config->getDetectedVCLVar(),
            $config->getDetectedOSVCLVar()
        );

        //generateUsingAgentLibrary vcl to match utilities
        $this->generateVCL(
            $this->getDetectionProvider()->getAllUtilities(),
            $config->getUtilityVCLVar(),
            $config->getUtilityVersionVCLVar(),
            $config->getUserAgentVCLVar(),
            $config->getDetectedVCLVar()
        );
        $this->generateSubEnd();
    }

    /**
     * Generate Sub Start
     *
     * @param string $subName
     * @param array $defaultVariables key/value pairs of default assignments
     */
    protected function generateSubStart($subName = 'devicedetect')
    {
        echo sprintf(
            'sub %s {%s',
            $subName,
            PHP_EOL
        );
    }

    /**
     * Generate Sub End
     */
    protected function generateSubEnd()
    {
        echo '}'.PHP_EOL;
    }

    /**
     * Generate VCL For Rules
     *
     * @param $rules
     * @param $variable
     */
    protected function generateVCL($rules, $typeVariable, $versionVariable, $userAgent, $detectedVariable)
    {
        $detectionProvider = $this->getDetectionProvider();
        $properties = $detectionProvider->getProperties();

        foreach ($rules as $name => $expr) {
            //print out $typeVariable assignment
            echo sprintf(
                "\tif(!%s && %s ~ \"(?i)%s\") {%s\t\tset %s = \"%s\";%s\t\tset %s = \"true\";%s",
                $detectedVariable,
                $userAgent,
                $expr,
                PHP_EOL,
                $typeVariable,
                $name,
                PHP_EOL,
                $detectedVariable,
                PHP_EOL
            );
            if (isset($properties[$name])) {
                //add version determination
                $properties[$name] = (array) $properties[$name];
                foreach($properties[$name] as $propertyMatchString) {
                    $propertyPattern = $detectionProvider->getPropertyPattern($propertyMatchString);
                    //print out $versionVariable assignment
                    echo sprintf(
                        "\t\tif(!%s && %s ~ \"(?i)%s\") {%s\t\t\tset %s = re.group.1;",
                        $versionVariable, $userAgent, $propertyPattern, PHP_EOL, $versionVariable
                    );
                    //add closing brace
                    echo sprintf("%s\t\t}%s", PHP_EOL, PHP_EOL);
                }
            }
            //add closing brace
            echo sprintf("\t}%s", PHP_EOL);
        }
    }
}
