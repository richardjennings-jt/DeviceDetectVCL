<?php

namespace DeviceDetectVCL;

use Mobile_Detect;
use Jenssegers\Agent\Agent;

class AgentDetectionProvider extends Agent implements DetectionProviderInterface
{
    /**
     * Return a regex string having done any work required to translate from detection provider original
     *
     * @param $propertyMatchString
     * @return string
     */
    public function getPropertyPattern($propertyMatchString)
    {
        return str_replace('[VER]', Mobile_Detect::VER, $propertyMatchString);
    }

    /**
     * @return array
     */
    public function getAllProperties()
    {
        return array_merge(
            parent::$properties,
            static::$additionalProperties
        );
    }

    /**
     * @return array
     */
    public function getAllPhones()
    {
        return $this->getPhoneDevices();
    }

    /**
     * @return array
     */
    public function getAllTablets()
    {
        return $this->getTabletDevices();
    }

    /**
     * @return array
     */
    public function getAllBrowsers()
    {
        return $this->mergeRules(
            static::$additionalBrowsers,
            static::$browsers
        );
    }

    /**
     * @return array
     */
    public function getAllOs()
    {
        return $this->mergeRules(
            static::$operatingSystems,
            static::$additionalOperatingSystems
        );
    }

    /**
     * @return array
     */
    public function getAllUtilities()
    {
        return static::$utilities;
    }
}
