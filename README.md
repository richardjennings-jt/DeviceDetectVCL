# Device Detection VCL Generator

Varnish / Fastly compatible VCL generator for Device Detection derived from the Mobile Detect and Agent PHP Libraries.

## TL;DR
Device and Version detection VCL configuration: [devicedetect.vcl](devicedetect.vcl)

## Example Usage
```
./bin/generate > devicedetect.vcl
```

## How it works

Regular expressions are extracted from [jenssegers / agent ](https://github.com/jenssegers/agent.git) PHP library, 
which in-turn utilises [serbanghita / Mobile-Detect](https://github.com/serbanghita/Mobile-Detect) providing support for 
both Desktop detection alongside Mobile detection capabilities. When bin/generate is executed, a VCL subroutine is output
on stdout composed of if statements defining req.http.X-UA-* variables according to the result of regular expression
matching against the User Agent.

The variables set are:

- req.http.X-UA-Phone eg. iPhone
- req.http.X-UA-Phone-Vs eg. 8_0
- req.http.X-UA-Tablet eg. iPad
- req.http.X-UA-Tablet-Vs eg. 6_0
- req.http.X-UA-Browser eg. Safari
- req.http.X-UA-Browser-Vs eg. 535.19
- req.http.X-UA-Os eg. AndroidOS
- req.http.X-UA-Os-Vs eg. 7_0_2
- req.http.X-UA-Utility eg. Watch
- req.http.X-UA-Utility-Vs


